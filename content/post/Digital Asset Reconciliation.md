+++
title = "Preserve Digital Memories"
date = "2017-05-30T16:56:43+02:00"
tags = ["images", "posts", "web sites"]
categories = ["Data mining", "discovery"]
menu = ""
banner = "banners/Spain-Plaza-de-Cibeles-Madrid-1018x460.jpg"
+++


## WHAT HAVE SOCIAL MEDIA DONE FOR YOU?  

Do not freely give your thoughts and images to those who profit off of you!
Publish your assets first to retain advantage!

DW84 Inc LLC Foundation Promotion Agent primary role is to publish client digital assets.